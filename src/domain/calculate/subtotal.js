module.exports = class Subtotal {

	constructor(count, sum) {
		this.count = count || 0;
		this.sum = sum || 0;
	}

	add(value) {
		return new Subtotal(this.count + 1, this.sum + value);
	}

	getCount() {
		return this.count;
	}

	getSum() {
		return this.sum;
	}
}
