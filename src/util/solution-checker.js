const Rx = require('rxjs/Rx');
const chalk = require('chalk');
const { almostEqual, consoleSupportsUnicode } = require('./utils');
const exerciseSolutionMap = require('./exercise-solutions');

const Symbols = (consoleSupportsUnicode()
	? { SUCCESS: '✔', ERROR: '✖' }
	: { SUCCESS: '√', ERROR: '×' }
);

exports.checkSolution = function(exerciseId, solution$, epsilon = 0.0000001, formatValue = (value) => value) {
	console.log(`\nChecking your solution for exercise ${chalk.yellow(exerciseId)}...\n`);

	if (!solution$) {
		console.log(chalk.red(`${Symbols.ERROR} You have not provided a solution yet!`));
		return;
	}

	if (typeof solution$.subscribe !== 'function') {
		console.log(chalk.red(`${Symbols.ERROR} Your solution is invalid because it cannot be observed! Try something that is a little more reactive ;)`));
		return;
	}

	checkSolution(solution$, exerciseSolutionMap.get(exerciseId), epsilon, formatValue).subscribe(
		() => {
			console.log(chalk.greenBright(`\n${Symbols.SUCCESS} Your solution is correct, awesome work! :D`));
			
			process.exit(0);
		},
		(errors) => {
			console.log(chalk.red(`\n${Symbols.ERROR} Unfortunately your solution is not correct... but don\'t worry, you\'ll get there!`));
			
			console.log('\nTo help you out a little, this is what went wrong:');
			errors.forEach((error) => console.log(' - ' + error));

			process.exit(1);
		}
	);
}

function checkSolution(solution$, expectedEvents, epsilon, formatValue) {
	
	const startTime = Date.now();

	const result = new Rx.ReplaySubject(1);

	let solutionStreamSubscription;

	function solutionPassed() {
		solutionStreamSubscription && solutionStreamSubscription.unsubscribe();
		result.next();
		result.complete();
	}

	function solutionFailed(errors) {
		solutionStreamSubscription && solutionStreamSubscription.unsubscribe();
		result.error(errors);
	}

	const expectedEventStack = [ ...expectedEvents ].reverse();

	let nextEventNumber = 1;

	function checkNextEvent(actualEvent) {
		const elapsedTime = Date.now() - startTime;

		const expectedEvent = expectedEventStack.pop();

		const eventNumber = nextEventNumber++;

		if (actualEvent.event === 'next') {
			console.log(` ${chalk.grey('-')} ${formatValue(actualEvent.value)}`);
		}

		const { success, errors } = matchEvents(Object.assign({}, actualEvent, { time: elapsedTime }), expectedEvent, eventNumber);

		if (!success) {
			solutionFailed([ errors ]);
		} else if (actualEvent.event === 'complete') {
			solutionPassed();
		}

	}

	function matchEvents(actual, expected, eventNumber) {
		function fail(...errors) {
			return { success: false, errors };
		}

		if (!expected) {
			return fail(`expected no more events after ${expectedEvents.length} events but got one (${chalk.yellow(eventToString(actual))}) anyway`);
		}

		if (actual.event !== expected.event) {
			return fail(`expected event #${eventNumber} to be ${chalk.yellow(eventToString(expected))}, bot got ${chalk.yellow(eventToString(actual))} instead`);
		}

		if (expected.event === 'next' && !isEqual(actual.value, expected.value, epsilon)) {
			return fail(`expected value for event #${eventNumber} to be ${chalk.yellow(expected.value)} instead of ${chalk.yellow(actual.value)}`);
		}

		if (expected.event === 'error' && !isEqual(actual.error, expected.error, epsilon)) {
			return fail(`expected error for event #${eventNumber} to be ${chalk.yellow(expected.error)} instead of ${chalk.yellow(actual.error)}`);
		}

		// TODO: Check timings... not enough time to do so... sorry about that.

		return { success: true }
	}

	solutionStreamSubscription = solution$.subscribe({
		next: (value) => checkNextEvent({ event: 'next', value }),
		error: (error) => checkNextEvent({ event: 'error', error }),
		complete: () => checkNextEvent({ event: 'complete' })
	});

	return result.asObservable();

}

function eventToString(event) {
	if (event.event === 'next') {
		return `[next:\`${event.value}\`]`;
	} else if (event.event === 'error') {
		return `[error:\`${event.error}\`]`;
	} else if (event.event === 'complete') {
		return '[complete]';
	}

	throw new Error(`${event.event} is not a valid Observable event`);
}

function isEqual(actual, expected, epsilon) {
	if (actual === expected) {
		return true;
	}
	if (typeof expected.equals === 'function') {
		return expected.equals(actual);
	}
	if (typeof expected === 'number' && typeof actual === 'number') {
		return almostEqual(actual, expected, epsilon);
	}
	if (typeof expected === 'string' && typeof actual === 'string') {
		return normalizeString(actual) === normalizeString(expected);
	}
	return false;
}

function normalizeString(value) {
	return value.replace(/\s+/g, ' ').trim().toLowerCase();
}
